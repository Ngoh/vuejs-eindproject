import axios from 'axios';

const url = '/api/thread'

class PostService{
    //get
    static getPosts(){  //hoeft geen Post object te creeren om deze functie te gebruiken, kan gewoon aangeroepen worden door PostService.getposts
        return new Promise(async(resolve, reject) => {
            try {
                const res = await axios.get(url)
                const data = res.data
                resolve(
                    data.map(post => ({
                        ...post
                    })) //loop door set van data
                )
            } catch (error) {
                reject(error)
            }
        })
    }
    //create
    static insertPost(text){
        return axios.post(url, {
            name: "thread post name", 
            title: "insert test",
            text: text
        })
    }
    //delete
    static deletePost(id){
        return axios.delete(url + "/" + id)
    }

    static updatePost(id, text){
        return axios.post(url + "/" + id, {
            text: text
        })
    }
}

export default PostService