import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import Dashboard from './views/Dashboard.vue'
import About from './views/About.vue'
import Main from './views/Main.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Profile from './views/Profile.vue'
import Community from './views/Community'
// import PostComponent from './components/PostComponent.vue'

Vue.use(VueRouter)
Vue.use(BootstrapVue)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


const routes = [
  {path: '/', component: Main},
  {path: '/Login', component: Login},
  {path: '/Register', component: Register},  
  {path : '/Dashboard', component: Dashboard},
  {path : '/Profile', component: Profile},
  {path : '/About', component: About},
  {path : '/Communities', component: Community}
]

const router = new VueRouter({
  routes,
  mode: 'history'
})
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  render: h=> h(App)
})

export default router

