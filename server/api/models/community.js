const mongoose = require('mongoose')

const communitySchema = new mongoose.Schema({
    name: {type: String, required: true},
    description: {type: String, required: true},
    private: {type: Boolean, required: true}

})

module.exports = mongoose.model('Community', communitySchema);