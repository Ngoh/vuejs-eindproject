const mongoose = require('mongoose')

// const commentSchema = new mongoose.Schema({
//     _id : mongoose.Schema.Types.ObjectId,
//     text: {type: String, required: [true, 'The content can not be empty']},
//     name: {type: String, required: [true, 'A comment must have an username']},
//     upvotes: {type: Number, required: true, default: 0},
//     downvotes: {type: Number, required: true, default: 0}
// })

const threadSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    name: {type: String, required: true},
    title: {type: String, required: true},
    text: {type: String, required: true}
    // upvotes: {type:Number, required: true},
    // downvotes: {type:Number, required: true},
    // comments: [commentSchema]

})

module.exports = mongoose.model('Thread', threadSchema);
