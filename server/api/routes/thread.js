const express = require('express')
const router = express.Router()
const Thread = require('../models/thread')
const mongoose = require('mongoose')

//Het ophalen van alle threads met bijbehorende unieke identifier, gebruikersnaam, titel, content, 
//totaal aantal upvotes van de thread en totaal aantal downvotes van de thread. 
//Merk op dat comments niet worden opgehaald. Voorzie een aparte API om deze threads op te halen ...
router.get('/', (req, res, next) => {
    Thread.find({}, { 
        // comments: 0 
    })
        .exec()
        .then(docs => {
            if (docs.length > 0) {
                res.status(200).json(docs)
            } else {
                res.status(404).json({
                    message: "No data found"
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
})

//alle thread sorted op comments
router.get('/sorted/comments', (req, res, next) => {
    Thread.find()
        .select({ comments: 0 })
        .exec()
        .then(docs => {
            if (docs.length > 0) {
                docs.sort((a, b) => (a.__v > b.__v) ? -1 : 1)
                console.log(docs)
                res.status(200).json(docs)
            } else {
                res.status(404).json({
                    message: "No data found"
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: errx
            })
        })
})

//alle thread sorted op upvotes
router.get('/sorted/upvotes', (req, res, next) => {
    Thread.find()
        .select({ comments: 0 })
        .exec()
        .then(docs => {
            if (docs.length > 0) {
                docs.sort((a, b) => (a.upvotes > b.upvotes) ? -1 : 1)
                console.log(docs)
                res.status(200).json(docs)
            } else {
                res.status(404).json({
                    message: "No data found"
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
})

//alle thread sorted op upvotes-downvotes
router.get('/sorted/karma', (req, res, next) => {
    Thread.find()
        .select({ comments: 0 })
        .exec()
        .then(docs => {
            if (docs.length > 0) {
                docs.sort((a, b) => ((a.upvotes - a.downvotes) > (b.upvotes - b.downvotes)) ? -1 : 1)
                console.log(docs)
                res.status(200).json(docs)
            } else {
                res.status(404).json({
                    message: "No data found"
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
})

//Find thread by Id
router.get('/:threadId', (req, res, next) => {
    const id = req.params.threadId
    Thread.findById(id).exec().then(doc => {
        console.log(doc)
        if (doc) {
            res.status(200).json({
                doc
            })
        } else {
            res.status(404).json({
                message: "No valid entry found for : " + id
            })
        }

    })
        .catch(err => {
            console.log(err)
            res.status(500).json({ error: err })
        })

})

//Het toevoegen van een nieuwe thread op basis van een verplichte gebruikersnaam, 
//verplichte titel en verplichte content in de vorm van platte tekst. 
//Een thread zal een unieke identifier nodig hebben. 
//Threads moeten op zijn minst in een MongoDB database worden opgeslagen.
router.post('/', (req, res, next) => {
    const thread = new Thread({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        title: req.body.title,
        text: req.body.text
        // upvotes: req.body.upvotes,
        // downvotes: req.body.downvotes
    })
    thread.save().then(result => {
        console.log(result)
        res.status(201).json({
            message: "Thread created"

        })
    }).catch(err => {
        console.log(err)
        res.status(500).json({
            error: err
        })
    })

})

//Post een comment
router.post('/:threadId', (req, res, next) => {
    const id = req.params.threadId
    Thread.findOne({ _id: id })
        .exec()
        .then(result => {
            console.log(result)
            result.comments.push({ _id: new mongoose.Types.ObjectId, text: req.body.text, name: req.body.name })
            result.save().then(result => {
                res.status(201).json({
                    message: "Comment created"
                })
            }).catch(err => {
                res.status(500).json({
                    error: err
                })
            })
        })
        .catch(err => {
            res.status(204).json({
                error: err
            })
        })
})

//Het aanpassen van de content van een thread op basis van de unieke identifier van de thread en de nieuwe content. 
//Merk op dat je de titel van een thread niet mag wijzigen.
router.put('/:threadId', (req, res, next) => {
    const id = req.params.threadId
    Thread.update({ _id: id }, { $set: { text: req.body.text } })
        .exec()
        .then(result => {
            console.log(result)
            res.status(200).json({
                message: "Updated thread succesfully",
                response: result
            })
        })
        .catch(err => {
            console.log(err)
            res.status(204).json({
                error: err
            })
        })
})

//Het verwijderen van een thread op basis van de unieke identifier van de thread. 
//Hierbij worden alle bijbehorende comments, upvotes en downvotes ook verwijderd.
router.delete('/:threadId', (req, res, next) => {
    const id = req.params.threadId
    Thread.deleteOne({ _id: id })
        .exec()
        .then(result => {
            if (result.deletedCount === 1) {
                res.status(200).json({
                    message: "Delete succesful"
                })
            } else {
                res.status(401).json({
                    message: "There was nothing to delete"
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
})

router.delete('/:threadId/:commentId', (req, res, next) => {
    const id = req.params.commentId
    Thread.findByIdAndUpdate(req.params.threadId, { $pull: {"comments" : {
        _id : req.params.commentId
    }}}, {safe: true, upsert: true})
    .exec()
    .then(result => {
            res.status(200).json({
                message: "Delete operation excecuted"
            })
    })
    .catch(err => {
        res.status(500).json({
            message: "Comment could not be deleted",
            error: err
        })
    })
})

module.exports = router