const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const cors = require('cors')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const User = require('../models/user')
router.use(cors())

process.env.SECRET_KEY = 'secret'

router.post("/register", (req, res) => {
    const userData ={
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        password: req.body.password,
        email: req.body.email
    }

    User.findOne({
        email : req.body.email
    })
    .then(user => {
        if(!user){
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                userData.password = hash
                User.create(userData)
                .then(user => {
                    res.status(200).json({status: user.email + ' registered'})
                })
                .catch(err => {
                    res.status(404).send('error :' + err)
                })
            })
        } else{ 
            res.status(404).json({error: 'User already exists'})
        }
    })
    .catch(err => {
        res.send('error :' + err )
    })
})

router.post('/login', (req, res) => {
    User.findOne({
        email: req.body.email
    })
    .then(user => {
        if(user){
            if (bcrypt.compareSync(req.body.password, user.password)) {
                const payload = {
                    _id: user._id,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    email: user.email
                }
                let token = jwt.sign(payload, process.env.SECRET_KEY, {
                    expiresIn: 1440
                })
                res.status(200).send(token)
            } else{
                res.status(404).json({error: 'User does not exist'})
            }
        } else{
            res.status(404).json({error: 'User does not exist'})
        }
    })
    .catch(err => {
        res.status(404).send('error : ' + err)
    })
})


module.exports = router