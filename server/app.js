const express = require('express')
const app = express()
const morgan = require('morgan')
const bodyparser = require('body-parser')
const mongoose = require('mongoose')

const userRoutes = require('./api/routes/user')
const threadRoutes = require('./api/routes/thread')
const port = process.env.PORT || 3000;

app.listen(port, () => console.log("Server started on " + port))

mongoose.connect('mongodb+srv://abc123456:abc123456@cluster0-8bp1z.mongodb.net/mongoAPI?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

app.use(morgan('dev'))
app.use(bodyparser.json())



//handling requests
app.use('/api/user', userRoutes)
app.use('/api/thread', threadRoutes)


if(process.env.NODE_ENV === 'production'){
    //static folder
    app.use(express.static(__dirname + '/public/'))

    //handle SPA
    app.get(/.*/, (req, res) => res.sendfile(__dirname + '/public/index.html'))
}


// //standard error handler
// app.use((req, res, next) => {
//     const error = new Error('Not found')
//     error.status = 404
//     next(error) //forward error request
// })

// app.use((error, req, res, next) => {
//     res.status(error.status || 500)
//     res.json({
//         error: {
//             message: error.message
//         }
//     }) 
// })

module.exports = app