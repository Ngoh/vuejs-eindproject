const mongoose = require('mongoose')
mongoose.Promise = global.Promise //ES6 promise

before(done => {
    mongoose.connect('mongodb+srv://abc123456:abc123456@cluster0-8bp1z.mongodb.net/mongoAPI?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    mongoose.connection
        .once("open", () => {
            done()
        })
        .on("error", error => {
            console.log("No connection", error)
        })
})

beforeEach((done) => {
    mongoose.connection.db.dropDatabase(() => {
        console.log("dropping database")
        done()
    });
}) //hooks dont go in testcases but mostly in helper files
