const mocha = require('mocha')
const assert = require('assert')
const Thread = require('../api/models/thread')
const mongoose = require('mongoose')

describe('Saving records to database', () => {
    it('creates a record in the database', () => {
        const thread = new Thread({
            _id: new mongoose.Types.ObjectId(),
            name: "Test Thread",
            title : "Title of test thread",
            text : "Content of test thread",
            upvotes : 6,
            downvotes : 9,
            comments: []
        })
    
        thread.save().then(res => {
            assert(!thread.isNew)
        })
    })
})


describe('Error trying to save record with no name', () => {
    it('Error trying to save a new thread because username is empty', () => {
        const thread = new Thread({
            _id: new mongoose.Types.ObjectId(),
            name: "",
            title : "Title of test thread",
            text : "Content of test thread",
            upvotes : 6,
            downvotes : 9,
            comments: []
        })
    
        thread.save().then(res => {
            assert(thread.isNew)
        })
    })
})

describe('Error trying to save record with no title', () => {
    it('Error trying to save a new thread because title is empty', () => {
        const thread = new Thread({
            _id: new mongoose.Types.ObjectId(),
            name: "123",
            title : "",
            text : "Content of test thread",
            upvotes : 6,
            downvotes : 9,
            comments: []
        })
    
        thread.save().then(res => {
            assert(thread.isNew)
        })
    })
})


describe('Error trying to save record with no text', () => {
    it('Error trying to save a new thread because text is empty', () => {
        const thread = new Thread({
            _id: new mongoose.Types.ObjectId(),
            name: "123",
            title : "123",
            text : "",
            upvotes : 6,
            downvotes : 9,
            comments: []
        })
    
        thread.save().then(res => {
            assert(thread.isNew)
        })
    })
})


describe('Read test', () => {
    let thread;
    beforeEach((done) => {
            thread = new Thread({
            _id: new mongoose.Types.ObjectId(),
            name: "Read test Thread",
            title : "Title of READ test thread",
            text : "Content of READ test thread",
            upvotes : 6969,
            downvotes : 6969,
            comments: []
        })
        thread.save()
            .then(() => {
                done()
            })
    })
    it('Read a thread', () => {
         Thread.find({name: "Read test Thread"})
            .then(result => {
                assert(thread._id.toString() === result[0]._id.toString())
            })
    })
})

describe('Delete test', () => {
    let thread;
    beforeEach((done) => {
            thread = new Thread({
            _id: new mongoose.Types.ObjectId(),
            name: "123",
            title : "Title of READ test thread",
            text : "Content of READ test thread",
            upvotes : 6969,
            downvotes : 6969,
            comments: []
        })
        thread.save()
            .then(() => {
                done()
            })
    })
    it('Delete a record out of the database', done => {
         Thread.findByIdAndDelete(thread._id)
            .then(() => Thread.findOne({name: "123"}))
            .then(thread === null)
            done()
    })
})

describe('Update test', () => {
    let thread;
    beforeEach((done) => {
            thread = new Thread({
            _id: new mongoose.Types.ObjectId(),
            name: "6969",
            title : "Title of READ test thread",
            text : "Content of READ test thread",
            upvotes : 6969,
            downvotes : 6969,
            comments: []
        })
        thread.save()
            .then(() => {
                done()
            })
    })
    it('Update a record and save it in the database', () => {
        thread.set('name', '69')
        thread.save()
            .then(() => Thread.find({}))
            .then(result => {
                assert(result[0].name !== '6969')
            })
    })
})