const chai = require("chai");
const chaiHttp = require("chai-http");
const assertArrays = require("chai-arrays");
const assert = require('assert')
const User = require('../api/models/user')
const mongoose = require('mongoose')

const server = require('../app')
const userEndpoint = "/user"

chai.should();
chai.use(chaiHttp);
chai.use(assertArrays);

const user = new User({
    _id : new mongoose.Types.ObjectId(),
    name: "Sylvester Stallone",
    pass: "Arnold Schwarzenegger",
})

describe('Save an user', () => {
    it('creates a record of new user and store it in the database', () => {
        user.save().then(res => {
            assert(!user.isNew)
        })
    })
})

describe('Error empty input', () => {
    it('Give an error trying to create an user with an empty input', done => {
        chai
        .request(server)
        .post(userEndpoint)
        .set("Content-Type", "application/json")
        .send({
            _id : new mongoose.Types.ObjectId(),
            name: "",
            pass: "Arnold Schwarzenegger",
        })
        .end((err, res) => {
            res.should.have.status(500)
            res.body.should.be.a("object")
            res.body.should.have.property("message").that.is.an("string")
            res.body.should.have
            .property("message")
            .equals("User validation failed: name: Path `name` is required.");
            done()
        })
    })
})

describe('Change password', () => {
    it('Success with password change with correct oldpass', done => {
        chai
        .request(server)
        .put(userEndpoint)
        .set("Content-Type", "application/json")
        .send({
            name : "Sylvester Stallone",
            pass : "Arnold Schwarzenegger",
            newpass : "Pass"
        })
        .end((err, res) => {
            res.body.should.be.a("object");
            res.body.should.have.property("message").that.is.an("string");
            res.body.should.have
              .property("message")
          });
          done();
    })
})

describe('Error trying to change password', () => {
    it('Should not change password because oldpassword is incorrect', done => {
        chai
        .request(server)
        .put(userEndpoint)
        .set("Content-Type", "application/json")
        .send({
            name : "Sylvester Stallone",
            pass : "Noob",
            newpass : "Pass"
        })
        .end((err, res) => {
            res.body.should.be.a("object");
            res.body.should.have.property("message").that.is.an("string");
            res.body.should.have
              .property("message")
          });
          done();
    })
})

describe('Error trying to delete', () => {
    it('Give an error trying to delete a record with wrong paass', done => {
        chai
        .request(server)
        .delete(userEndpoint)
        .set("Content-Type", "application/json")
        .send({
            name : "Sylvester Stallone",
            pass : "123"
        })
        .end((err, res) => {
            res.should.have.status(401);
            res.body.should.be.a("object");
            res.body.should.have.property("message").that.is.an("string");
            res.body.should.have
              .property("message")
              .equals("User could not be deleted because the password did not match");
          });
          done();
    })
})

describe('Delete an user with right password', () => {
    it('Delete the user record if the password is correct', done => {
        chai
        .request(server)
        .delete(userEndpoint)
        .set("Content-Type", "application/json")
        .send({
            name : "Sylvester Stallone",
            pass : "Pass"
        })
        .end((err, res) => {
            res.body.should.be.a("object");
            res.body.should.have.property("message").that.is.an("string");
          });
          done();
    })
})



// it("should give an apartmentId when posting an apartment", done => {
//     chai
//       .request(server)
//       .post(apartmentEndpoint)
//       .set("Content-Type", "application/json")
//       .set("Authorization", "Bearer " + tokenUser1)
//       .send({
//         Description: "Testing apartment user 1",
//         StreetAddress: "Lovensdijkstraat 61",
//         PostalCode: "5000 XD",
//         City: "Breda"
//       })
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.body.should.be.a("object");
//         res.body.should.have.property("message").that.is.an("string");
//         res.body.should.have
//           .property("message")
//           .equals("Apartment was succesfully saved");
//         res.body.should.have.property("id");

//         apartmentUser1 = res.body.id;
//         done();
//       });
//   });